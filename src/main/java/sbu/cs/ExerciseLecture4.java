package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n)
    {
        if(n==0) return 1;
        if(n<0) return -1;
        long res = 1;
        for (int i=1; i<=n; i++){
            res *=i;
        }
        return res;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n)
    {
        long a = 1;
        long b = 1;
        int counter = 1;
        while (counter <=n){
            if(counter==n) return a;
            a += b;
            counter++;
            if(counter<=n) {
                if(counter==n) return b;
                b += a;
                counter++;
            }
        }
        return 0;
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word)
    {
        String rev = "";
        for (int i=0; i<word.length(); i++){
            rev = rev + word.substring(word.length()-i-1,word.length()-i);
        }
        return rev;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line)
    {
        String rev = "";
        for (int i=0; i<line.length(); i++){
            rev = rev + line.substring(line.length()-i-1,line.length()-i);
        }
        if(line.equals(rev)){
            return true;
        }
        else{
            return false;
        }
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2)
    {
        char[][] plot = new char[str1.length()][str2.length()];
        for (int i=0; i<str1.length();i++){
            for(int j=0; j<str2.length(); j++){
                if(str1.charAt(i)== str2.charAt(j)) {
                    plot[i][j]='*';
                }
                else plot[i][j]=' ';
            }
        }
        return plot;
    }
}
