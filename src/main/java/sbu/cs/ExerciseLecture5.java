package sbu.cs;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length)
    {
        String pass="";
        String chars = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i<length; i++){
            pass+=chars.charAt((int)(Math.random()*26));
        }
        return pass;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length)
    {
        String pass="";
        String sChars = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
        String digits = "0123456789";
        String oChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char[] chars = new char[length];
        chars[0] = sChars.charAt((int)(Math.random()*sChars.length()));
        chars[1] = digits.charAt((int)(Math.random()*digits.length()));
        String aChars = sChars + digits + oChars;
        for (int i = 2; i<length; i++)
        {
            chars[i] = aChars.charAt((int)(Math.random()*aChars.length()));
        }
        Random generator = new Random();
        int randomIndex = generator.nextInt(chars.length);
        for(int i=0; i<length; i++)
        {
            pass += chars[(i + randomIndex)%length];
        }
        return pass;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        return false;
    }
}
