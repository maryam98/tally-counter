package sbu.cs;

import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr)
    {
        int sum = 0;
        for(int i=0; i<arr.length; i+=2){
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr)
    {
        int n = arr.length;
        int[] reversedArray = new int[n];
        for (int i = 0; i<n; i++){
            reversedArray[i]=arr[n-i-1];
        }
        return reversedArray;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {
        if(m1[0].length==m2.length) {
            double[][] product = new double[m1.length][m2[0].length];
            try {
                for (int i = 0; i < product.length; i++) {
                    for (int j = 0; j < product[0].length; j++) {
                        for(int k=0; k<m1[0].length; k++){
                            product[i][j] += m1[i][k] * m2[k][j];
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return product;
        }
        else {
            System.out.println("The number of columns of the first matrix must be equal to the number of rows of the second matrix");
            return null;
        }
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names)
    {
        int n = names.length;
        List<List<String>> listOfLists = new ArrayList<List<String>>(n);
        for(int i=0; i<names.length; i++){
            ArrayList<String> items = new ArrayList<>();
            for(int j=0; j<names[i].length; j++){
                items.add(names[i][j]);
            }
            listOfLists.add(items);
        }
        return listOfLists;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n)
    {
        List<Integer> factors = new ArrayList<>();
        if(n==1){
            factors.add(1);
            return factors;
        }
        else if(n<1){
            factors.add(-1);
            return factors;
        }
        int factor=2;
        while(factor<=n)
        {
            if(n%factor==0)
            {
                factors.add(factor);
                n=n/factor;
            }
            else factor++;
        }
        return factors;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {
        List<String> words = new ArrayList<>();
        String[] wordsArray = line.split(" ");
        for(int i=0; i<wordsArray.length; i++)
        {
            words.add(wordsArray[i]);
        }
        return words;
    }
}
