package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    int value;

    @Override
    public void count() {
        this.value++;
    }

    @Override
    public int getValue() {
        return this.value;
    }

    @Override
    public void setValue(int value) throws IllegalValueException  {
        try {
            this.value = value;
        }catch(IllegalValueException  e){
            System.out.println("Value must be integer!");
        }
    }
}
